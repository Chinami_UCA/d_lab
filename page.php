<?php get_header(); ?>
	<div id="mainSide">
		<?php $page = get_post(); ?>
			<div class="pageName">
			  <h1 ><?php echo $page->post_title; ?></h1>
			  <p class="pageNameEng"><?php echo $page->post_name; ?></p>
			 </div>
				<div class="container">
					<div class="breadcrumbs"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
					<div class="mainBox">
						<div id="pageBox">
						　　　　	<?php if(have_posts()): ?>
								<?php while(have_posts()): the_post(); ?>
									<div class="pageBoxIn">
										<?php the_content(); ?>
									</div>
								<?php endwhile; ?>
								<?php endif; ?>
						</div>
						<div><a class="moreBtn" href="<?php bloginfo("url")?>">詳しく見る</a></div> 
					</div>
				</div>
	</div>

<?php get_footer(); ?>