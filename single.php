<?php get_header(); ?>
<div id="main">
		<div class="singleNameBox"><div class="container">
			<?php
				$category = get_the_category();
				$cat_name = $category[0]->cat_name;
				$cat_slug = $category[0]->category_nicename;
			?>
			<p class="singleName"><?php echo $cat_name; ?></p>
		</div></div>
	<div class="container">
		<?php get_template_part('breadcrumbs'); ?>
		<div class="singleMainBox">
				<div class="postBox">
						<?php if(have_posts()): ?>
						<?php while(have_posts()): the_post(); ?>
							<div class="postNameBox">
								<p class="date"><?php echo get_post_time('Y年m月'); ?></p>
								<h1 class="singlePostTit"><?php the_title(); ?></h1>
							</div>
							<div class="eachArticle">
								<?php the_content(); ?>
							</div>	
						<?php endwhile; ?>
						<?php endif; ?>
				</div>
			</div>
		</div></div>
	</div>
</div>

<?php get_footer(); ?>