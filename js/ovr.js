
/**
 * addEventFunc 関数
 *
 * イベントの追加用
 * @since 2013-05-12
 * @version 1.0.0
 */
var addEventFunc = function(func){
try {
window.addEventListener('load', func, false);
} catch (e) {
window.attachEvent('onload', func);
}
}



/**
 * objCheck 関数
 *
 * オブジェクトチェック用関数
 * @returns true 有効なオブジェクト
 * @returns false 無効なオブジェクト（undefined,null,''）
 */
function objCheck(obj) {
return (typeof obj == 'undefined' || obj == null || obj == '') ? false : true;
}


/**
 * smartRollover 関数
 *
 * 画像のロールオーバーを画像名で判断して設定する
 * outの画像設置のみでOK。ovrは勝手に読み込んでくれる。_ovr.jpg
 * @since 2013-05-12
 * @version 1.0.0
 */
var smartRollover = {
init: function(){
smartRollover.setup('img');
smartRollover.setup('input');
},
setup: function(tag) {
var ovr = '_ovr.';
var out = '_out.';

var obj = document.getElementsByTagName(tag);
if(objCheck(obj) == false) return false;

var preload = [];
for(var i=0; i<obj.length; i++) {
if(obj[i].src.indexOf(out) != -1){
var src = obj[i].src;
obj[i].ext = src.slice(src.lastIndexOf('.')+1, src.length);
obj[i].nimg = src;
obj[i].oimg = src.replace(out + obj[i].ext, ovr + obj[i].ext);

preload[i] = new Image();
preload[i].src = obj[i].oimg;

obj[i].onmouseover = function() {
this.setAttribute('src', this.oimg);
};
obj[i].onmouseout = function() {
this.setAttribute('src', this.nimg);
};
}
}
return false;
}
}
addEventFunc(smartRollover.init);



