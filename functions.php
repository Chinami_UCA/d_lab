<?php
add_theme_support( 'post-thumbnails' );

function length_get_the_excerpt($length){
    global $post; $post_temporaly = '';

    if (!$length) {
        $length = 120;
    }

    $post_id = get_the_ID();
    $mojionly = strip_tags($post->post_content);
    if(mb_strlen($mojionly ) > $length) {$t = '... <a class="eachBlogExcerpt" href="'. get_permalink( get_the_ID() ) . '">詳しく見る</a>';}
    $content =  mb_substr($mojionly, 0, $length);
    $content .= $t;
    if($post_temporaly) {$post = $post_temporaly;}
    return $content;
}

// thumbnail自動表示
function catch_that_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];
  
    if(empty($first_img)){ //Defines a default image
		$blankimg = get_template_directory_uri(). "/img/blog_thum01.jpg";
        $first_img = $blankimg;
    }
return $first_img;
}

/*抜粋設定*/
function new_excerpt_mblength($length) {
     return 200;
}
add_filter( 'excerpt_mblength', 'new_excerpt_mblength' );
function new_excerpt_more( $more ) {
    return '... <a href="'. get_permalink( get_the_ID() ) . '">続きを見る</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );



/*yarpp設定*/
//ヘッダー部分のCSS消去
add_action('wp_print_styles','lm_dequeue_header_styles');
function lm_dequeue_header_styles(){ wp_dequeue_style('yarppWidgetCss'); }
 
//フッター部分のCSS消去
add_action('get_footer','lm_dequeue_footer_styles');
function lm_dequeue_footer_styles(){ wp_dequeue_style('yarppRelatedCss'); }
 
//サムネイルを有効にした場合に呼び出されるCSS消去
add_action('wp_print_styles','lm_dequeue_header_styles1');
function lm_dequeue_header_styles1(){ wp_dequeue_style('yarpp-thumbnails-yarpp-thumbnail'); }
add_action('get_footer','lm_dequeue_footer_styles1');
function lm_dequeue_footer_styles1(){ wp_dequeue_style('yarpp-thumbnails-yarpp-thumbnail'); }




//現在のページ数の取得
function show_page_number() {
    global $wp_query;

    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $max_page = $wp_query->max_num_pages;
    echo $paged;  
}






?>