<?php get_header(); ?>
	<div id="mainSideIn">
		<div class="catNameBox">
			<?php
				$category_slug = get_queried_object()->category_nicename;
			?>
     		<h1><?php single_cat_title( '', true ); ?></h1>
     	</div>
     	<div class="container">
	     	<div class="mainBox">
				<div class="row">
					<div class="col-md-9 col-sm-9"><div class="catMain">
						<?php if(!is_paged()):?>
									<div class="catDes featured"><?php echo category_description(); ?></div>
						<?php endif;?>

						<?php if(have_posts()): ?>
						<?php while(have_posts()): the_post(); ?>
							<div class="eachBlog"><div class="row">
							<div class="col-md-3 col-sm-3 col-xs-4"><div class="eachLeft">
								<?php if(get_the_post_thumbnail()) :?>
						    		<p class="eachImg"><a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail("thumbnail"); ?></p></a>
						    	<?php else:?>
						    		<p class="eachImg"><a href="<?php echo the_permalink(); ?>"><img class="img-responsive img-thumbnail" src="<?php echo get_template_directory_uri(); ?>/img/"></a></p>
						    	<?php endif;?>
								</div></div>						
								<div class="col-md-8 col-sm-8 col-xs-8"><div class="eachRight">
									<p class="date"><?php echo get_post_time('Y年m月d日'); ?></p>
									<p class="tit"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></p>
									<?php the_excerpt(); ?>
								</div></div>
							</div></div>
						<?php endwhile; ?>
						<?php endif; ?>
						<?php if(function_exists('wp_pagenavi')) {wp_pagenavi(); } ?>
					</div></div>
					<div class="col-md-3 col-sm-3"><?php get_sidebar(); ?></div>
				</div>
			</div>
		</div>		
	</div>
<?php get_footer(); ?>