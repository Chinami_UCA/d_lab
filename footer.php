<footer id="footer">


</footer>
<?php wp_footer(); ?>

<script type="text/javascript" charset="utf-8">


$(window).load(function() {
$('.flexslider').flexslider({
	directionNav: false,
  controlNav: false,
	slideshowSpeed: 6000
});
});
$(function () {
    $('.logo1_1').textillate({
      //繰り返し
      loop: false,
      // アニメーションの間隔時間
      minDisplayTime:1000,
      // アニメーション開始までの遅延時間
      initialDelay:800,
      // アニメーションの自動スタート
      autoStart: true,
  
      // 開始時のアニメーション設定
      in: {
        // エフェクトの指定
        effect: 'fadeIn',
        // 遅延時間の指数
        delayScale: 1,
        // 文字ごとの遅延時間
        delay:0,
        // true:アニメーションをすべての文字に同時適用
        sync: true,
        // true:文字表示がランダムな順に表示される
        shuffle: false
      },
    });
    $('.logo1_2').textillate({
      //繰り返し
      loop: false,
      // アニメーションの間隔時間
      minDisplayTime:1000,
      // アニメーション開始までの遅延時間
      initialDelay:1300,
      // アニメーションの自動スタート
      autoStart: true,
  
      // 開始時のアニメーション設定
      in: {
        // エフェクトの指定
        effect: 'fadeIn',
        // 遅延時間の指数
        delayScale: 1,
        // 文字ごとの遅延時間
        delay: 20,
        // true:アニメーションをすべての文字に同時適用
        sync: false,
        // true:文字表示がランダムな順に表示される
        shuffle: false
      },
    });
    $('.logo2_1').textillate({
      //繰り返し
      loop: false,
      // アニメーションの間隔時間
      minDisplayTime: 1000,
      // アニメーション開始までの遅延時間
      initialDelay: 3500,
      // アニメーションの自動スタート
      autoStart: true,
  
      // 開始時のアニメーション設定
      in: {
        // エフェクトの指定
        effect: 'fadeIn',
        // 遅延時間の指数
        delayScale: 1,
        // 文字ごとの遅延時間
        delay:0,
        // true:アニメーションをすべての文字に同時適用
        sync: true,
        // true:文字表示がランダムな順に表示される
        shuffle: false
      },
    });
    $('.logo2_2').textillate({
      // //繰り返し
      loop: false,
      // アニメーションの間隔時間
      minDisplayTime: 1000,
      // アニメーション開始までの遅延時間
      initialDelay: 4000,
      // アニメーションの自動スタート
      autoStart: true,
  
      // 開始時のアニメーション設定
      in: {
        // エフェクトの指定
        effect: 'fadeIn',
        // 遅延時間の指数
        delayScale: 1,
        // 文字ごとの遅延時間
        delay: 10,
        // true:アニメーションをすべての文字に同時適用
        sync: false,
        // true:文字表示がランダムな順に表示される
        shuffle: false
      },
    });
})
$(function () {
    setTimeout('rect()'); //アニメーションを実行
});

function rect() {
    $('.topMenuBtn').animate({
        marginTop: '-=8px'
    }, 600).animate({
        marginTop: '+=8px'
    }, 500);
    setTimeout('rect()', 1600); //アニメーションを繰り返す間隔
}
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 800; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});

$(".spMenuBtn").click( function(){ 
    $("#spNav").toggle(500);
});

$(".aboutusMore1").click( function(){ 
    $(".eachMen2nd1").toggle(100);
});
$(".aboutusMore2").click( function(){ 
    $(".eachMen2nd2").toggle(100);
});
$(".aboutusMore3").click( function(){ 
    $(".eachMen2nd3").toggle(100);
});
$(".aboutusMore4").click( function(){ 
    $(".eachMen2nd4").toggle(100);
});
$(".aboutusMore5").click( function(){ 
    $(".eachMen2nd5").toggle(100);
});
$(".aboutusMore6").click( function(){ 
    $(".eachMen2nd6").toggle(100);
});

//headanavスクロールするとnav表示

$(function() {
    var nav = $('#header');
    var target = $('#header').offset().top;
    $(window).scroll(function(){
      var current_scroll = $(this).scrollTop();
      if(target < current_scroll){
        nav.css({
          'position' : 'fixed',
          'top' : 0,
          'left' : 0,
          'width' : 100 + '%'
        });
      }else{
        nav.css({
          'position' : 'relative',
          'top' : 'auto',
          'left' : 'auto',
        });
      }
    });
});


</script>
</body></html>
